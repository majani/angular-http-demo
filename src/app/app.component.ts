import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from './post.model';
import { PostsService } from './posts.service';
import { NgForm } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  defaultTitle = 'Title'
  loadedPosts: Post[] = [];
  postData: Post;
  isFetching = false;
  @ViewChild('postForm', {static:true}) postForm: NgForm;
  error = null;
  err= new Subject<string>();       // 1..Declare a Subject to handle errors. Good practive when you will want to display the errors in a lot of places
  private errorSub: Subscription; // 3. Declare a Subscription variable to be used in subscribing to the err Subject

  constructor(
    private http: HttpClient,
    private postSvc: PostsService
    
    
    ) { }

  ngOnInit() {
    this.err.subscribe(errorMessage => {      // 4.. subscribe to the Subject using the Subscription variable in order to display errors
      this.error= errorMessage;
    })
    this.isFetching = true
    this.onFetchPosts();
  }

  onCreatePost(postData: Post) {
    // Send Http request
   this.postSvc.createAndStorePost( postData.title, postData.content).subscribe(
     (response) => {
       this.postForm.reset();
       this.onFetchPosts();
       console.log(response);
     },
     error => {
       this.err.next(error.message);      //// 2.. Pass the Error message to the Subject in the errror function
     }
     )
   
  }

  onFetchPosts() {
    this.isFetching = true
    this.postSvc.fetchPosts().subscribe(
      (response) => {
        this.isFetching = false
        this.loadedPosts = response
        console.log(response);
      },
     error => {
       this.isFetching = false;
      this.error = error.message
    }
    )
    
   
  }

  // onFetchPosts(){
  //   this.http.get<Post []>('https://ng-complete-guide-233a4.firebaseio.com/posts.json').subscribe(
  //     response => {
  //       this.loadedPosts = response;
  //       console.log(response);
  //     }
  //   )
  // }

  onClearPosts() {
    // Send Http request
    this.postSvc.deletePosts().subscribe(
      (response) => {
        this.onFetchPosts();
        console.log(response);
      }
    )
  }

  onHandleError(){
    this.error = null;
  }

  ngOnDestroy(){
    this.errorSub.unsubscribe();
  }
}
