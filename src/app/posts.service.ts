import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Post } from './post.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(
    private http: HttpClient
  ) { }

  createAndStorePost(title: string, content: string) {
    const postData: Post = { title: title, content: content }
   return this.http.post<Post>('https://ng-complete-guide-233a4.firebaseio.com/posts.json', postData);

  }

  fetchPosts() {
    let searchParams = new HttpParams();
    searchParams = searchParams.append('print','pretty');
    searchParams = searchParams.append('pretty','please');
    return this.http.get('https://ng-complete-guide-233a4.firebaseio.com/posts.json', 
    { headers : new HttpHeaders({"Authorization": "basicAuthHeaderString"}),
      params : searchParams
  
  }
    ).pipe(
      map((responseData: { [key: string]: Post }) => {
        const postArray: Post[] = [];
        for (const key in responseData) {
          if (responseData.hasOwnProperty(key)) {
            postArray.push({ ...responseData[key], id: key });
          }
        }
        return postArray;
      }


      ))
  }

  deletePosts(){
    return this.http.delete('https://ng-complete-guide-233a4.firebaseio.com/posts.json');
  }
}
